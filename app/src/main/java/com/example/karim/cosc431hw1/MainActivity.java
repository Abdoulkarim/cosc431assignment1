package com.example.karim.cosc431hw1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText FirstNum= (EditText)findViewById(R.id.FirstNum);
        final EditText SecondNum= (EditText)findViewById(R.id.SecondNum);
        final EditText Result= (EditText)findViewById(R.id.Result);
        Button CalBtn= (Button)findViewById(R.id.CalBtn);
        Result.setFocusable(false);


        CalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num1= Integer.parseInt(FirstNum.getText().toString());
                int num2= Integer.parseInt(SecondNum.getText().toString());
                Result.setText(num1 + num2 + " ");

            }
        });
    }
}
